package com.latif.utils;

/**
 * Created by LATIF on 9/4/2014.
 */
public final class NumberUtils {

    //--- THOUSAND CEIL ------------------------------------------------------------------------------------------------

    /**
     * Menggenapkan ke atas dalam format ribuan. Misal 1,987 -> 2,000;  25,042 -> 26,000; 45,000 -> 45,000
     *
     * @param d
     * @return
     */
    public static double thousandCeil(double d) {
        return Math.ceil(d / 1000) * 1000;
    }

    /**
     * Menggenapkan ke atas dalam format ribuan. Misal 1,987 -> 2,000;  25,042 -> 26,000; 45,000 -> 45,000
     *
     * @param f
     * @return
     */
    public static float thousandCeil(float f) {
        return (float) thousandCeil((double) f);
    }

    /**
     * Menggenapkan ke atas dalam format ribuan. Misal 1,987 -> 2,000;  25,042 -> 26,000; 45,000 -> 45,000
     *
     * @param i
     * @return
     */
    public static int thousandCeil(int i) {
        return (int) thousandCeil((double) i);
    }

    /**
     * Menggenapkan ke atas dalam format ribuan. Misal 1,987 -> 2,000;  25,042 -> 26,000; 45,000 -> 45,000
     *
     * @param l
     * @return
     */
    public static long thousandCeil(long l) {
        return (long) thousandCeil((double) l);
    }

    //--- THOUSAND FLOOR -----------------------------------------------------------------------------------------------

    /**
     * Menggenapkan ke bawah dalam format ribuan. Misal 1,987 -> 1,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param d
     * @return
     */
    public static double thousandFloor(double d) {
        return Math.floor(d / 1000) * 1000;
    }

    /**
     * Menggenapkan ke bawah dalam format ribuan. Misal 1,987 -> 1,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param f
     * @return
     */
    public static float thousandFloor(float f) {
        return (float) thousandFloor((double) f);
    }

    /**
     * Menggenapkan ke bawah dalam format ribuan. Misal 1,987 -> 1,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param i
     * @return
     */
    public static int thousandFloor(int i) {
        return (int) thousandFloor((double) i);
    }

    /**
     * Menggenapkan ke bawah dalam format ribuan. Misal 1,987 -> 1,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param l
     * @return
     */
    public static long thousandFloor(long l) {
        return (long) thousandFloor((double) l);
    }

    //--- THOUSAND ROUND -----------------------------------------------------------------------------------------------

    /**
     * Menggenapkan dalam format ribuan. misal 1,987 -> 2,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param d
     * @return
     */
    public static double thousandRound(double d) {
        return Math.round(d / 1000) * 1000;
    }

    /**
     * Menggenapkan dalam format ribuan. misal 1,987 -> 2,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param f
     * @return
     */
    public static float thousandRound(float f) {
        return (float) thousandRound((double) f);
    }

    /**
     * Menggenapkan dalam format ribuan. misal 1,987 -> 2,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param i
     * @return
     */
    public static int thousandRound(int i) {
        return (int) thousandRound((double) i);
    }

    /**
     * Menggenapkan dalam format ribuan. misal 1,987 -> 2,000;  25,042 -> 25,000; 45,000 -> 45,000
     *
     * @param l
     * @return
     */
    public static long thousandRound(long l) {
        return (long) thousandRound((double) l);
    }

    //--- DECIMAL ROUND ------------------------------------------------------------------------------------------------

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma.
     * decimalRound(19.12105, 2) -> 19.12.
     * decimalRound(19.12105, 2) -> 19.121.
     * decimalRound(19.12105, 2) -> 19.1211.
     *
     * @param f
     * @param digit
     * @return
     */
    public static float decimalRound(float f, int digit) {
        return (float) decimalRound((double) f, digit);
    }

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma.
     * decimalRound(19.12105, 2) -> 19.12.
     * decimalRound(19.12105, 2) -> 19.121.
     * decimalRound(19.12105, 2) -> 19.1211.
     *
     * @param d
     * @param digit
     * @return
     */
    public static double decimalRound(double d, int digit) {
        double comma = (double) Math.pow(10d, (double) digit);
        return Math.round(d * comma) / comma;
    }

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma. Pengenapan ke bawah.
     * decimalRound(19.12105, 2) -> 19.12.
     * decimalRound(19.12105, 2) -> 19.121.
     * decimalRound(19.12105, 2) -> 19.121.
     *
     * @param f
     * @param digit
     * @return
     */
    public static float decimalFloor(float f, int digit) {
        return (float) decimalFloor((double) f, digit);
    }

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma. Pengenapan ke bawah.
     * decimalRound(19.12105, 2) -> 19.12.
     * decimalRound(19.12105, 2) -> 19.121.
     * decimalRound(19.12105, 2) -> 19.121.
     *
     * @param d
     * @param digit
     * @return
     */
    public static double decimalFloor(double d, int digit) {
        double comma = (double) Math.pow(10d, (double) digit);
        return Math.floor(d * comma) / comma;
    }

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma. Pengenapan ke atas.
     * decimalRound(19.12105, 2) -> 19.12.
     * decimalRound(19.12105, 2) -> 19.121.
     * decimalRound(19.12105, 2) -> 19.121.
     *
     * @param f
     * @param digit
     * @return
     */
    public static float decimalCeil(float f, int digit) {
        return (float) decimalCeil((double) f, digit);
    }

    /**
     * Menggenapkan bilangan dari berapa digit angka dibelakang koma. Penggenapan ke atas.
     * decimalRound(19.12105, 2) -> 19.13.
     * decimalRound(19.12105, 2) -> 19.122.
     * decimalRound(19.12105, 2) -> 19.1211.
     *
     * @param d
     * @param digit
     * @return
     */
    public static double decimalCeil(double d, int digit) {
        double comma = (double) Math.pow(10d, (double) digit);
        return Math.ceil(d * comma) / comma;
    }

    //---- INTEGER -------------------------------------------------------------

    /**
     * Jika o adalah Integer returnkan hasil casting Integer.
     * Jika o adalah String returnkan konversi string ke Integer.
     *
     * @param o
     * @return null jika o bukan Number. atau tidak ada nilainya.
     */
    public static Integer valueInt(Object o) {
        if (o instanceof Integer) {
            return (Integer) o;
        } else if (o instanceof Number) {
            return new Integer(((Number) o).intValue());
        } else if (o instanceof String) {
            try {
                return Integer.valueOf((String) o);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Jika o adalah Number returnkan hasil casting int.
     * Jika o adalah String returnkan konversi string ke int.
     *
     * @param o
     * @return 0 jika o bukan Number. atau tidak ada nilainya.
     */
    public static int parseInt(Object o) {
        if (o instanceof Integer) {
            return ((Integer) o).intValue();
        } else if (o instanceof Number) {
            return ((Number) o).intValue();
        } else if (o instanceof String) {
            try {
                return Integer.parseInt(o + "");
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            return 0;
        }
    }

    //---- LONG ----------------------------------------------------------------

    /**
     * Jika o adalah Number returnkan hasil casting Long.
     * Jika o adalah Long returnkan hasil casting Long.
     * Jika o adalah String returnkan konversi string ke Long.
     *
     * @param o
     * @return
     */
    public static Long valueLong(Object o) {
        if (o instanceof Long) {
            return (Long) o;
        } else if (o instanceof Number) {
            return new Long(((Number) o).longValue());
        } else if (o instanceof String) {
            try {
                return Long.valueOf((String) o);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Jika o adalah Number returnkan hasil casting long.
     * Jika o adalah Long returnkan hasil casting long.
     * Jika o adalah String returnkan konversi string ke long.
     *
     * @param o
     * @return
     */
    public static long parseLong(Object o) {
        if (o instanceof Long) {
            return ((Long) o).longValue();
        } else if (o instanceof Number) {
            return ((Number) o).longValue();
        } else if (o instanceof String) {
            try {
                return Long.parseLong((String) o);
            } catch (NumberFormatException e) {
                return 0L;
            }
        } else {
            return 0L;
        }
    }

    //---- FLOAT ---------------------------------------------------------------

    /**
     * Jika o adalah Integer returnkan hasil casting Integer.
     * Jika o adalah Long returnkan hasil casting Long.
     * Jika o adalah Float returnkan hasil casting Float.
     * Jika o adalah String returnkan konversi string ke Float.
     *
     * @param o
     * @return
     */
    public static Float valueFloat(Object o) {
        if (o instanceof Float) {
            return (Float) o;
        } else if (o instanceof Number) {
            return new Float(((Number) o).floatValue());
        } else if (o instanceof String) {
            try {
                return Float.valueOf((String) o);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Jika o adalah Number returnkan hasil casting float.
     * Jika o adalah Float returnkan hasil casting float.
     * Jika o adalah String returnkan konversi string ke float.
     *
     * @param o
     * @return
     */
    public static float parseFloat(Object o) {
        if (o instanceof Float) {
            return ((Float) o).floatValue();
        } else if (o instanceof Number) {
            return ((Number) o).floatValue();
        } else if (o instanceof String) {
            try {
                return Float.parseFloat((String) o);
            } catch (NumberFormatException e) {
                return 0f;
            }
        } else {
            return 0f;
        }
    }

    public static void main(String[] args) {
        Number n = 10.5f;
        System.out.println(n.intValue());
        System.out.println(n.longValue());
        System.out.println(n.floatValue());
        System.out.println(n.doubleValue());
    }
}
