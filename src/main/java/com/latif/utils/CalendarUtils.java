package com.latif.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by LATIF on 6/11/2015.
 */
public final class CalendarUtils {

    public final static String[] BULANS = {
            "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    };

    public final static String[] MONTHS = {
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    };

    /**
     * Date dijadikan Calendar.
     *
     * @param date
     * @return
     */
    public static Calendar toCalendar(Date date) {
        if (date == null) {
            return null;
        }

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Waktu dalam milisecond dijadikan Calendar.
     *
     * @param milis milisecond in String
     * @return
     */
    public static Calendar toCalendar(Long milis) {
        if (milis == null) {
            return null;
        }
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(milis);
        return calendar;
    }

    /**
     * Date dijadikan tanggal dalam String.
     *
     * @param date
     * @param format
     * @return
     */
    public static String toString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    /**
     * Calendar dijadikan tanggal dalam String.
     *
     * @param cal
     * @param format
     * @return
     */
    public static String toString(Calendar cal, String format) {
        return toString(cal.getTime(), format);
    }

    //----------- DIFF -------------------------------------------------------------------------------------------------

    /**
     * Dapatkan selisih bulan antara 2 calendar.
     * cal1 - cal2. Jadi cal1 harus setelah cal2.
     *
     * @param cal1
     * @param cal2
     * @return
     */
    public static int diffMonth(Calendar cal1, Calendar cal2) {
        return (cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR)) * 12 + (cal1.get(Calendar.MONTH) - cal2.get(Calendar.MONTH));
    }

    /**
     * Dapatkan selisih bulan antara 2 calendar. cal1 - cal2. jadi cal1 harus setelah cal2.
     * Perhitungan selisih dihitung berdasarkan periode tanggal tiap bulan dari cal2.<p>
     * Example:
     * <ul>
     * <li>cal1: 5-5-2014, cal2:7-1-2014 => 3</li>
     * <li>cal1: 7-5-2014, cal2:7-1-2014 => 4</li>
     * <li>cal1: 10-5-2014, cal2:7-1-2014 => 4</li>
     * </ul>
     *
     * @param cal1
     * @param cal2
     * @return
     */
    public static int diffPeriodMonth(Calendar cal1, Calendar cal2) {
        int diff = diffMonth(cal1, cal2);
        //buat calendar yang pada bulan+tahun yang sama dengan cal1 tetapi tanggalnya adalah cal2.
        //jika tanggal cal2 ternyata lebih dari akhir bulan cal1 maka jadikan akhir bulan sebagai tanggalnya.
        //ex: cal1: 5-5-2014, cal2:7-1-2014 => c:7-5-2014
        //ex: cal1: 4-4-2014, cal2:31-1-2014 => c:30-4-2014 --> jadikan akhir bulan sebagai tanggal
        Calendar c = (Calendar) cal1.clone();
        c.set(Calendar.DATE, cal2.get(Calendar.DATE) >= c.getActualMaximum(Calendar.DATE) ? c.getActualMaximum(Calendar.DATE) : cal2.get(Calendar.DATE));
        //Jika cal1 sebelum c atau belum genap periodenya, maka dikurangi 1.
        return cal1.before(c) ? diff - 1 : diff;
    }

}
