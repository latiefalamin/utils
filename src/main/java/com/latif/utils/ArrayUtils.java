package com.latif.utils;

/**
 * Created by LATIF on 6/11/2015.
 */
public final class ArrayUtils {

    /**
     * Apakah object ada yang sama pada salah satu array os.
     *
     * @param object
     * @param os
     * @return
     */
    public static boolean equals(Object object, Object... os) {
        if (object == null || os == null) {
            return false;
        }

        for (Object o : os) {
            if (object == o || object.equals(o)) {
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        String [] s = {"a","b","C"};
        equals("a",s);
    }
}
