package com.latif.utils;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 7/10/13
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
public final class KonversiTerbilang {

    public static String konversiTerbilang(long number) {
        String terbilang = "";
        terbilang = konversiTerbilangTrilyunan(number).trim();
        return terbilang;
    }

    private static String konversiTerbilangSatuan(long satuan) {
        String terbilangSatuan = "";
        switch ((int) satuan) {
            case 0:
                terbilangSatuan = "nol";
                break;
            case 1:
                terbilangSatuan = "satu";
                break;
            case 2:
                terbilangSatuan = "dua";
                break;
            case 3:
                terbilangSatuan = "tiga";
                break;
            case 4:
                terbilangSatuan = "empat";
                break;
            case 5:
                terbilangSatuan = "lima";
                break;
            case 6:
                terbilangSatuan = "enam";
                break;
            case 7:
                terbilangSatuan = "tujuh";
                break;
            case 8:
                terbilangSatuan = "delapan";
                break;
            case 9:
                terbilangSatuan = "sembilan";
                break;
            default:
                terbilangSatuan = "";
                break;
        }
        return terbilangSatuan;
    }

    private static String konversiTerbilangPuluhan(long puluhan) {
        String terbilangPuluhan = "";
        if (puluhan < 100 && puluhan >= 0) {
            int bilPuluhan = (int) (puluhan / 10);
            int bilSatuan = (int) (puluhan % 10);

            if (bilPuluhan == 0) {
                terbilangPuluhan = konversiTerbilangSatuan(bilSatuan);
            } else {
                if (bilPuluhan == 1) {
                    if (bilSatuan == 0) {
                        terbilangPuluhan = "sepuluh";
                    } else if (bilSatuan == 1) {
                        terbilangPuluhan = "sebelas";
                    } else {
                        terbilangPuluhan = konversiTerbilangSatuan(bilSatuan) + " belas";
                    }
                } else {
                    if (bilPuluhan >= 2 && bilPuluhan <= 9) {
                        terbilangPuluhan = konversiTerbilangSatuan(bilPuluhan) + " puluh";
                    }
                    if (bilSatuan > 0) {
                        terbilangPuluhan += " " + konversiTerbilangSatuan(bilSatuan);
                    }
                }
            }
        }
        return terbilangPuluhan.trim();
    }

    private static String konversiTerbilangRatusan(long ratusan) {
        String terbilangRatusan = "";
        if (ratusan >= 0 && ratusan < 1000) {
            int bilRatusan = (int) (ratusan / 100);
            int bilPuluhan = (int) (ratusan % 100);

            if (bilRatusan == 0) {
                terbilangRatusan = konversiTerbilangPuluhan(bilPuluhan);
            } else {
                if (bilRatusan == 1) {
                    terbilangRatusan = "seratus";
                } else if (bilRatusan >= 2 && bilRatusan <= 9) {
                    terbilangRatusan = konversiTerbilangSatuan(bilRatusan) + " ratus";
                }
                if (bilPuluhan > 0) {
                    terbilangRatusan += " " + konversiTerbilangPuluhan(bilPuluhan);
                }
            }
        }
        return terbilangRatusan.trim();
    }

    private static String konversiTerbilangRibuan(long ribuan) {
        String terbilangRibuan = "";
        if (ribuan >= 0 && ribuan < 1000000) {
            int bilRibuan = (int) (ribuan / 1000);
            int bilRatusan = (int) (ribuan % 1000);

            if (bilRibuan == 0) {
                terbilangRibuan = konversiTerbilangRatusan(bilRatusan);
            } else {
                if (bilRibuan == 1) {
                    terbilangRibuan = "seribu";
                } else if (bilRibuan >= 2 && bilRibuan <= 999) {
                    terbilangRibuan = konversiTerbilangRatusan(bilRibuan) + " ribu";
                }
                if (bilRatusan > 0) {
                    terbilangRibuan += " " + konversiTerbilangRatusan(bilRatusan);
                }
            }
        }
        return terbilangRibuan.trim();
    }

    private static String konversiTerbilangJutaan(long jutaan) {
        String terbilangJutaan = "";
        if (jutaan >= 0 && jutaan < 1000000000) {
            int bilJutaan = (int) (jutaan / 1000000);
            int bilRibuan = (int) (jutaan % 1000000);

            if (bilJutaan == 0) {
                terbilangJutaan = konversiTerbilangRibuan(bilRibuan);
            } else {
                terbilangJutaan = konversiTerbilangRatusan(bilJutaan) + " juta";
                if (bilRibuan > 0) {
                    terbilangJutaan += " " + konversiTerbilangRibuan(bilRibuan);
                }
            }
        }
        return terbilangJutaan.trim();
    }

    private static String konversiTerbilangMilyaran(long milyaran) {
        String terbilangMilyaran = "";
        if (milyaran >= 0 && milyaran < 1000000000000L) {
            int bilMilyaran = (int) (milyaran / 1000000000);
            int bilJutaan = (int) (milyaran % 1000000000);

            if (bilMilyaran == 0) {
                terbilangMilyaran = konversiTerbilangJutaan(bilJutaan);
            } else {
                terbilangMilyaran = konversiTerbilangRatusan(bilMilyaran) + " milyar";
                if (bilJutaan > 0) {
                    terbilangMilyaran += " " + konversiTerbilangJutaan(bilJutaan);
                }
            }
        }
        return terbilangMilyaran.trim();
    }

    private static String konversiTerbilangTrilyunan(long trilyunan) {
        String terbilangTrilyunan = "";
        if (trilyunan >= 0 && trilyunan < 1000000000000000L) {
            int bilTrilyunan = (int) (trilyunan / 1000000000000L);
            long bilMilyaran = trilyunan % 1000000000000L;

            if (bilTrilyunan == 0) {
                terbilangTrilyunan = konversiTerbilangMilyaran(bilMilyaran);
            } else {
                terbilangTrilyunan = konversiTerbilangRatusan(bilTrilyunan) + " trilyun";
                if (bilMilyaran > 0) {
                    terbilangTrilyunan += " " + konversiTerbilangMilyaran(bilMilyaran);
                }
            }
        }
        return terbilangTrilyunan.trim();
    }
}
