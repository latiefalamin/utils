package com.latif.utils;

import java.util.Collection;

/**
 * Created by LATIF on 6/11/2015.
 */
public class CollectionUtils {

    /**
     * Menambahlan item pada collection tanpa terduplikat.
     *
     * @param collection
     * @param o
     * @param <C>
     * @return
     */
    public static <C extends Collection> C addItemWithoutDuplicated(C collection, Object o) {
        if (o == null) {
            return collection;
        }
        if (collection.contains(o)) {
            return collection;
        }
        collection.add(o);
        return collection;
    }

}
