package com.latif.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Latief
 * Date: 5/27/12
 * Time: 8:04 PM
 */
public final class BooleanUtils {

    /**
     * Mengubah ke tipe Boolean.
     * Tipe Boolean akan otomatis tercasting menjai Boolean.
     * Tipe String "true", "t", "1", "false", "f", "0" diubah menjadi Boolean. Not Case Sensitive.
     * Tipe Number 0 atau 1 diubah jadi Boolean.
     * @param o
     * @return
     */
    public static Boolean toBoolean(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof Boolean) {
            return (Boolean) o;
        } else if (o instanceof String) {
            String s = ((String) o).trim();
            if ("true".equalsIgnoreCase(s) || "t".equalsIgnoreCase(s) || "1".equals(s)) {
                return Boolean.TRUE;
            } else if ("false".equalsIgnoreCase(s) || "f".equalsIgnoreCase(s) || "0".equals(s)) {
                return Boolean.FALSE;
            } else {
                return null;
            }
        } else if (o instanceof Number) {
            int i = ((Number) o).intValue();
            if (i == 0) {
                return Boolean.FALSE;
            } else if (i == 1) {
                return Boolean.TRUE;
            }
        }

        return null;
    }

}
